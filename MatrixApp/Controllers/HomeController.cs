﻿using Common.BL;
using MatrixApp.DTO;
using MatrixApp.Services;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace MatrixApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAnecdoteService _anecdoteService;
        public HomeController(ILogger<HomeController> logger, IAnecdoteService anecdoteService)
        {
            _logger = logger;
            _anecdoteService = anecdoteService;
        }
        public IActionResult Index()
        {
            return View();
        }   

        public IActionResult Anecdotes()
        {
            ViewBag.Anecdotes = _anecdoteService.GetAnecdotes();
            return View();
        }

        public IActionResult Task()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}