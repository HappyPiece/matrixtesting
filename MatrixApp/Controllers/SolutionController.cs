﻿using Common.BL;
using MatrixApp.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MatrixApp.Controllers
{
    [Route("api/solve")]
    [Controller]
    public class SolutionController : Controller
    {
        [HttpPost]
        public async Task<IActionResult> Solve([FromBody] TaskInput input)
        {
            //await Task.Delay(5000);
            if (!ModelState.IsValid)
            {
                return BadRequest("Data is terribly wrong");
            }
            bool result;
            try
            {
                result = Solution.SearchMatrix(input.Matrix, input.Target);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(result);
        }
    }
}
