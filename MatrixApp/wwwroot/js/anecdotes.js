﻿$(document).ready(init);

let activeAnecdoteNumber;
let anecdoteCount;

function init() {
    activeAnecdoteNumber = 0;
    anecdoteCount = $(".anecdote").length;
    if (anecdoteCount == 0) {
        $("#anecdote-container").append("<div>Sorry, no anecdotes today :(</div>");
    }
    setListeners();
    displayAnecdote();
    manageButtons();
}

function displayAnecdote() {
    $(".anecdote").addClass("d-none");
    $(`#${activeAnecdoteNumber}`).toggleClass("d-none");
}

function setListeners() {
    $("#btn-forward").on("click", () => { navigateAnecdote(1) });
    $("#btn-back").on("click", () => { navigateAnecdote(-1) });
}

function navigateAnecdote(offset) {
    activeAnecdoteNumber = limitIndex(offset);
    console.log(activeAnecdoteNumber);
    displayAnecdote();
    manageButtons();
}

function manageButtons() {
    if (activeAnecdoteNumber == 0) {
        $("#btn-back").attr("disabled", true);
    }
    else {
        $("#btn-back").attr("disabled", false);
    }

    if (activeAnecdoteNumber == anecdoteCount - 1) {
        $("#btn-forward").attr("disabled", true);
    }
    else {
        $("#btn-forward").attr("disabled", false);
    }
}

function limitIndex(offset) {
    let number = activeAnecdoteNumber + offset;
    number = Math.min(number, anecdoteCount);
    number = Math.max(number, 0);
    return number;
}
