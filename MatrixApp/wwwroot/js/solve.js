﻿const baseAddress = "https://localhost:7259"

$(document).ready(init);

function init() {
    setListeners();
    $.ajaxSetup({
        contentType: 'application/json'
    });
}


function setListeners() {
    $("button[type='submit']").on("click", solve)
}

function solve() {
    let rawValue = $("#matrix").val();
    let inputMatrix = rawValue.length == 0 ? [] : rawValue.split(/\r?\n|\r|\n/g).map(function (x) {
        return x.length == 0 ? [] : x.split(' ').map(function (y) {
            return parseInt(y);
        });
    });
    //console.log(inputMatrix);
    let target = parseInt($("#target").val());
    let content = JSON.stringify({ matrix: inputMatrix, target: target });
    console.log(content);
    let result = $.post(baseAddress + "/api/solve", content);
    result.done(function (data) {
        console.log(data);
        $("#result").val(data);
    }).fail(function ($xhr) {
        if ($xhr.status == 400) {
            console.log($xhr.responseText);
            $("#result").val(($xhr.responseText));
            //$("#result").val("Некорректные данные");
        }
    });
}


