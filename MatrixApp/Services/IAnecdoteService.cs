﻿namespace MatrixApp.Services
{
    public interface IAnecdoteService
    {
        public List<string> GetAnecdotes();


    }
}
