﻿namespace MatrixApp.DTO
{
    public class TaskInput
    {
        public int[][] Matrix { get; set; }
        public int Target { get; set; }
    }
}
