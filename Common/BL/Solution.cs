﻿using System.Collections.Generic;

namespace Common.BL
{
    public static class Solution
    {
        public static bool SearchMatrix(int[][] matrix, int target)
        {
            CheckInput(matrix, target);

            int ROWS = matrix.Length;
            int COLS = matrix[0].Length;
            int top = 0, bot = ROWS - 1;
            int row = 0;

            while (top <= bot)
            {
                row = (top + bot) / 2;
                if (target > matrix[row][COLS - 1]) top = row + 1;
                else if (target < matrix[row][0]) bot = row - 1;
                else break;
            }

            if (top > bot) return false;

            int l = 0, r = COLS - 1;

            while (l <= r)
            {
                int m = (l + r) / 2;
                if (target > matrix[row][m]) l = m + 1;
                else if (target < matrix[row][m]) r = m - 1;
                else return true;
            }

            return false;
        }

        private static void CheckInput(int[][] matrix, int target)
        {
            _checks.ForEach(x => x(matrix, target));
        }

        private static readonly List<Action<int[][], int>> _checks = new()
        {
            (matrix, target) =>
            {
                if (matrix.Length < 1 || matrix.Length > 100) throw new ArgumentException("Matrix n size out of bounds");
                for (int i = 0; i < matrix.Length; i ++)
                {
                    if (matrix[i].Length < 1 || matrix[i].Length > 100) throw new ArgumentException("Matrix m size out of bounds");
                    if (i != 0 && matrix[i].Length != matrix[i-1].Length)
                    {
                        foreach (var row in matrix)
                        {
                            foreach (var element in row)
                            {
                                Console.Write(element + " ");
                            }
                            Console.WriteLine("\r\n");
                        }
                        throw new ArgumentException("Matrix m size is inconsistent");
                    }
                }
            },
            (matrix, target) =>
            {
                for (int i = 0; i < matrix.Length; ++i)
                {
                    for (int j = 0; j < matrix[i].Length; ++j)
                    {
                        if (j != 0 && matrix[i][j-1] > matrix[i][j]) throw new ArgumentException("Matrix is badly sorted");
                    }
                    if (i != 0 && matrix[i-1][^1] >= matrix[i][0]) throw new ArgumentException($"Matrix is badly sorted");
                }
                foreach (var i in matrix)
                {
                    foreach (var j in i)
                    {
                        if (j < -10000 || j > 10000) throw new ArgumentException("Matrix element size out of bounds");
                    }
                }
            }
        };
    }
}
