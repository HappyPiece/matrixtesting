﻿using Common.BL;

int target = int.Parse(Console.ReadLine());
int[][] matrix = Array.Empty<int[]>();
string input;
while (!string.IsNullOrEmpty(input = Console.ReadLine()))
{
    matrix = matrix.Append(input.Split().Select(x => int.Parse(x)).ToArray()).ToArray();
}

Console.WriteLine(Solution.SearchMatrix(matrix, target));
Console.ReadLine();