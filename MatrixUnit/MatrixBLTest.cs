using Common.BL;
using static MatrixUnit.MatrixBLTestData;

namespace MatrixUnit
{
    /// <summary>
    ///                                                         ����������                                                
    ///     
    ///     ��� �����, ������� � �������� ��� ���� ������ ����� ���� ��������������� � ������������ � ���� ���� �������� �������,
    ///  ������� ���������� ������, ��������������� ���������������� ��������.
    ///     ��� ����������� �������, ������ ��� �� ��������, ��� ������������ ����� ���������� �� �������� �������� ������,
    ///  �� xUnit, ������� � ���������, �� ������������ ����������� �������� ������ (� ������� �� NUnit). �������, ����� � ���������
    ///  ������� ������������ ��� ������������, � ������� ����������� � ������, ������� ���������, ����������, ��� ��� ���������,
    ///  � ����� ���� ����������� � �������� ������, ����� ������������� � ���, ��� �� �� ���������, ���� �� �� ��� ��������������.
    ///  ��������:
    ///     SuppliedValidData_ReturnsResult => SuppliedMatrixContainingEqualElements_ReturnsResult
    ///     SuppliedInvalidData_ThrowsArgumentException => SuppliedEmptyMatrix_ThrowsArgumentException
    /// 
    ///     All tests I could come up with for this task could be parametrized and packed into two test methods,
    ///  that use data supplied in corresponding classes.
    ///     It's not perfect, since the task has been also meant to review test naming,
    ///  but xUnit framework I'm using does not natively support it (as against NUnit). To offset this unfortunate situation to some
    ///  extent, I wrote comments for test data, describing what it is meant to check. One could simply substitute
    ///  a general description given in a test method name with a particular one mentioned as comment and
    ///  gain perspective on how the method itself was intended to be named. For example:
    ///     SuppliedValidData_ReturnsResult => SuppliedMatrixContainingEqualElements_ReturnsResult
    ///     SuppliedInvalidData_ThrowsArgumentException => SuppliedEmptyMatrix_ThrowsArgumentException
    /// </summary>
    public class MatrixBLTest
    {
        

        [Theory, ClassData(typeof(ValidTestData))]
        public void SuppliedValidData_ReturnsResult(int[][] matrix, int target, bool expected)
        {
            var result = Solution.SearchMatrix(matrix, target);

            Assert.Equal(result, expected);
        }

        [Theory, ClassData(typeof(InvalidTestData))]
        public void SuppliedInvalidData_ThrowsArgumentException(int[][] matrix, int target, string message)
        {
            var act = new Action(() => Solution.SearchMatrix(matrix, target));

            Assert.Equal(Assert.Throws<ArgumentException>(act).Message, message);
        }
    }
} 