using Common;
using System.Collections;

namespace MatrixUnit
{
    public class MatrixBLTestData
    {
        public class ValidTestData : TestData
        {
            public override IEnumerator<object[]> GetEnumerator()
            {
                yield return new object[] // target element is present in the matrix
                {
                    new int[][] { new int[] { 1, 3, 5, 7 }, new int[] { 10, 11, 16, 20 }, new int[] { 23, 30, 34, 60 } }, 3, true
                };
                yield return new object[] // target element is not present in the matrix
                {
                    new int[][] { new int[] { 1, 3, 5, 7}, new int[] { 10, 11, 16, 20 }, new int[] { 23, 30, 34, 60 } }, 13, false
                };
                yield return new object[] // target element is the first in the matrix
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } }, 1, true
                };
                yield return new object[] // target element is the last in the matrix
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } }, 9, true
                };
                yield return new object[] // target element is close to value present in the matrix
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } }, 10, false
                };
                yield return new object[] // matrix element is adjacent to lower value limit
                {
                    new int[][] { new int[] { -10000, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } }, -10000, true
                };
                yield return new object[] // matrix element is adjacent to upper value limit
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 10000 } }, 10000, true
                };
                yield return new object[] // matrix contains equal elements in one row
                {
                    new int[][] { new int[] { 1, 3, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 10000 } }, 10000, true
                };
            }


        }

        public class InvalidTestData : TestData
        {
            public override IEnumerator<object[]> GetEnumerator()
            {
                yield return new object[] // matrix is empty
                {
                    Array.Empty<int[]>(), default(int), "Matrix n size out of bounds"
                };
                yield return new object[] // matrix m size is greater then maximum
                {
                    new int[][] { Enumerable.Range(0, 101).ToArray() }, default(int), "Matrix m size out of bounds"
                };
                yield return new object[] // matrix n size is greater then maximum
                {
                    Enumerable.Repeat(new int[] { 1, 2, 3 }, 101).ToArray(), default(int), "Matrix n size out of bounds"
                };
                yield return new object[] // matrix rows are inconsistent length
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5 }, new int[] { 7, 8, 9 } }, default(int), "Matrix m size is inconsistent"
                };
                yield return new object[] // matrix contains element below minimum
                {
                    new int[][] { new int[] { -10001, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } }, default(int), "Matrix element size out of bounds"
                };
                yield return new object[] // matrix contains element greater then maximum
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 10001 } }, default(int), "Matrix element size out of bounds"
                };
                yield return new object[] // row contains elements not in non-decreasing order
                {
                    new int[][] { new int[] { 1, 3, 2 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } }, default(int), "Matrix is badly sorted"
                };
                yield return new object[] // adjacent elements in different rows contain equal elements
                {
                    new int[][] { new int[] { 1, 2, 3 }, new int[] { 3, 5, 6 }, new int[] { 7, 8, 9 } }, default(int), "Matrix is badly sorted"
                };
            }
        }
    }
}