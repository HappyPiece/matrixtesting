using MatrixApp.DTO;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;
using System.Reflection;
using System.Text;
using System.Text.Json;
using static MatrixUnit.MatrixAPITestData;

namespace MatrixIntegration
{
    public class MatrixAPITest
    {
        private readonly HttpClient _httpClient = new HttpClient() { BaseAddress = new Uri(APITestConstants._baseAddress) };

        [Fact]
        public async Task RequestedNonExistentEndpoint_ReturnsNotFound()
        {
            var responce = await _httpClient.GetAsync("/api/aboba");

            Assert.Equal(404, (int)responce.StatusCode);
        }

        [Fact]
        public async Task SuppliedWrongMethod_ReturnsMethodNotAllowed()
        {
            var responce = await _httpClient.GetAsync("/api/solve");

            Assert.Equal(405, (int)responce.StatusCode);
        }

        [Fact]
        public async Task SuppliedUnconditionalData_ReturnsBadRequest()
        {
            var content = APITestUtilities.FormContent(new { bebra = "aboba" });

            var responce = await _httpClient.PostAsync("/api/solve", content);

            Assert.Equal(400, (int)responce.StatusCode);
        }

        [Fact]
        public async Task SuppliedUnconditionalData_ReturnsCorrectMessage()
        {
            var content = APITestUtilities.FormContent(new { bebra = "aboba" });

            var responce = await _httpClient.PostAsync("/api/solve", content);

            Assert.Equal("Data is terribly wrong", await responce.Content.ReadAsStringAsync());
        }

        [Fact]
        public async Task SuppliedWrongContentType_ReturnsUnsupportedMediaType()
        {
            var content = new StringContent(JsonSerializer.Serialize(new TaskInput
            {
                Matrix = new int[][] { new int[] { 1, 3, 5, 7 }, new int[] { 10, 11, 16, 20 }, new int[] { 23, 30, 34, 60 } },
                Target = 3
            }), Encoding.UTF8, "text/html");

            var responce = await _httpClient.PostAsync("/api/solve", content);

            Assert.Equal(415, (int)responce.StatusCode);
        }

        [Fact]
        public async Task SuppliedValidData_ReturnsOk()
        {
            var content = new TaskInput
            {
                Matrix = new int[][] { new int[] { 1, 3, 5, 7 }, new int[] { 10, 11, 16, 20 }, new int[] { 23, 30, 34, 60 } },
                Target = 3
            };

            var responce = await _httpClient.PostAsync("/api/solve", APITestUtilities.FormContent(content));

            Assert.Equal(200, (int)responce.StatusCode);
        }

        [Theory, ClassData(typeof(ValidTestData))]
        public async Task SuppliedValidData_ReturnsResult(int[][] matrix, int target, bool expected)
        {
            var content = new TaskInput
            {
                Matrix = matrix,
                Target = target
            };

            var responce = await _httpClient.PostAsync("/api/solve", APITestUtilities.FormContent(content));

            Assert.Equal(expected, JsonSerializer.Deserialize(await responce.Content.ReadAsStreamAsync(), typeof(bool), APITestConstants._jsonSerializerOptions));
        }

        [Fact]
        public async Task SuppliedInvalidData_ReturnsBadRequest()
        {
            var content = new TaskInput
            {
                Matrix = new int[][] { new int[] { 1, 3, 5 }, new int[] { 10, 11, 16, 20 }, new int[] { 23, 30, 34, 60 } },
                Target = default(int)
            };

            var responce = await _httpClient.PostAsync("/api/solve", APITestUtilities.FormContent(content));

            Assert.Equal(400, (int)responce.StatusCode);
        }

        [Theory, ClassData(typeof(InvalidTestData))]
        public async Task SuppliedInvalidData_ReturnsFailureReason(int[][] matrix, int target, string message)
        {
            var content = new TaskInput
            {
                Matrix = matrix,
                Target = target
            };

            var responce = await _httpClient.PostAsync("/api/solve", APITestUtilities.FormContent(content));

            Assert.Equal(message, await responce.Content.ReadAsStringAsync());
        }
    }

    public static class APITestUtilities
    {
        public static StringContent FormContent<T>(T model)
            => new(JsonSerializer.Serialize(model), Encoding.UTF8, APITestConstants._jsonType);
    }

    public static class APITestConstants
    {
        public const string _baseAddress = "https://localhost:7259";
        public static readonly JsonSerializerOptions _jsonSerializerOptions = new() { PropertyNameCaseInsensitive = true };
        public const string _jsonType = "application/json";
        public const string _htmlType = "text/html";
    }
}