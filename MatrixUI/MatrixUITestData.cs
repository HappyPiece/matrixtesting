using Common;
using System.Collections;

namespace MatrixUnit
{
    public class MatrixUITestData
    {
        public class ValidTestData : TestData
        {
            public override IEnumerator<object[]> GetEnumerator()
            {
                yield return new object[] // target element is present in the matrix
                {
                    "1 3 5 7\r\n10 11 16 20\r\n23 30 34 60","3","true"
                };
                yield return new object[] // target element is not present in the matrix
                {
                     "1 3 5 7\r\n10 11 16 20\r\n23 30 34 60","13","false"
                };
                yield return new object[] // target element is the first in the matrix
                {
                     "1 2 3\r\n4 5 6\r\n7 8 9","1","true"
                };
                yield return new object[] // target element is the last in the matrix
                {
                     "1 2 3\r\n4 5 6\r\n7 8 9","9","true"
                };
                yield return new object[] // target element is close to value present in the matrix
                {
                     "1 2 3\r\n4 5 6\r\n7 8 9","10","false"
                };
                yield return new object[] // matrix element is adjacent to lower value limit
                {
                     "-10000 2 3\r\n4 5 6\r\n7 8 9","-10000","true"
                };
                yield return new object[] // matrix element is adjacent to upper value limit
                {
                     "1 2 3\r\n4 5 6\r\n7 8 10000","10000","true"
                };
                yield return new object[] // matrix contains equal elements in one row
                {
                     "1 3 3\r\n4 5 6\r\n7 8 10000","10000","true"
                };
            }
        }

        public class InvalidTestData : TestData
        {
            public override IEnumerator<object[]> GetEnumerator()
            {
                yield return new object[] // matrix is empty
                {
                    "",default(int),"Matrix n size out of bounds"
                };
                yield return new object[] // one of the matrix's rows is empty 
                {
                     "1 2 3\r\n\r\n7 8 9",default(int),"Matrix m size out of bounds"
                };
                yield return new object[] // matrix m size is greater then maximum
                {
                     string.Join(" ", Enumerable.Range(0,101).ToArray()),default(int),"Matrix m size out of bounds"
                };
                yield return new object[] // matrix n size is greater then maximum
                {
                    string.Join("\r\n", Enumerable.Repeat("1 2 3",101).ToArray()),default(int),"Matrix n size out of bounds"
                };
                yield return new object[] // matrix rows are inconsistent length
                {
                     "1 2 3\r\n4 5\r\n7 8 9",default(int),"Matrix m size is inconsistent"
                };
                yield return new object[] // matrix contains element below minimum
                {
                     "-10001 2 3\r\n4 5 6\r\n7 8 9",default(int),"Matrix element size out of bounds"
                };
                yield return new object[] // matrix contains element greater then maximum
                {
                     "1 2 3\r\n4 5 6\r\n7 8 10001",default(int),"Matrix element size out of bounds"
                };
                yield return new object[] // row contains elements not in non-decreasing order
                {
                     "1 3 2\r\n4 5 6\r\n7 8 9",default(int),"Matrix is badly sorted"
                };
                yield return new object[] // adjacent elements in different rows contain equal elements
                {
                     "1 2 3\r\n3 5 6\r\n7 8 9",default(int),"Matrix is badly sorted"
                };
            }
        }
    }
}