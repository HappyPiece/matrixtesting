﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixUI
{

    public static partial class UITestExtensions {
        public static string GetValue(this IWebElement element)
        {
            return element.GetAttribute("value");
        }
    }
}
