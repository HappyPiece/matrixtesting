﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixUI
{
    public class MatrixUIUtilities
    {
        public class TaskPage
        {
            private readonly IWebDriver _driver;
            public const string URI = "https://localhost:7259/home/task";
            public TaskPage(IWebDriver driver)
            {
                _driver = driver;
                _driver.Navigate().GoToUrl(URI);
                MatrixInput.Clear();
            }

            public string Title => _driver.Title;
            public string PageSource => _driver.PageSource;
            public IWebElement TargetInput => _driver.FindElement(By.Id("target"));
            public IWebElement ResultField => _driver.FindElement(By.Id("result"));
            public IWebElement MatrixInput => _driver.FindElement(By.Id("matrix"));
            public IWebElement SubmitButton => _driver.FindElement(By.Id("submit"));

            public void EnterMatrix(string input)
            {
                MatrixInput.SendKeys(input);
            }
            public void EnterTarget(string input)
            {
                TargetInput.SendKeys(input);
            }
            public void Submit()
            {
                SubmitButton.Click();
            }
            public string GetResult()
            {
                return ResultField.GetValue();
            }

            public bool TargetInputIsPresent()
            {
                try
                {
                    var element = TargetInput;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            public bool MatrixInputIsPresent()
            {
                try
                {
                    var element = MatrixInput;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            public bool ResultFieldIsPresent()
            {
                try
                {
                    var element = ResultField;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            public bool SubmitButtonIsPresent()
            {
                try
                {
                    var element = SubmitButton;
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }
    }
}
