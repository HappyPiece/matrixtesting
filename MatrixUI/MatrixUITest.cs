using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.DevTools;
using System.Reflection;
using System.Text.Json;
using Xunit.Sdk;
using static MatrixUI.MatrixUIUtilities;
using static MatrixUnit.MatrixUITestData;

namespace MatrixUI
{
    /// <summary>
    /// ����� �������� � �������������� ������� Page Object;
    /// �����, ����������� ����������� ��������, ��������� � ����� MatrixUIUtilities
    /// </summary>
    public class MatrixUITest : IDisposable
    {
        private readonly IWebDriver _driver;
        private readonly TaskPage _page;

        public MatrixUITest()
        {
            _driver = new ChromeDriver();
            _page = new(_driver);
        }


        [Fact]
        public void RequestedPage_ReturnsViewWithCorrectTitle()
        {
            Assert.Equal("������ - MatrixApp", _page.Title);
        }

        [Fact]
        public void RequestedPage_ReturnsViewWithMatrixInput()
        {
            Assert.True(_page.MatrixInputIsPresent());
        }

        [Fact]
        public void RequestedPage_ReturnsViewWithTargetInput()
        {
            Assert.True(_page.TargetInputIsPresent());
        }

        [Fact]
        public void RequestedPage_ReturnsViewWithResultField()
        {
            Assert.True(_page.ResultFieldIsPresent());
        }

        [Fact]
        public void RequestedPage_ReturnsViewWithSubmitButton()
        {
            Assert.True(_page.SubmitButtonIsPresent());
        }

        [Theory, ClassData(typeof(ValidTestData))]
        public void SuppliedValidData_ReturnsResult(string matrix, string target, string result)
        {
            _page.EnterMatrix(matrix);
            _page.EnterTarget(target);
            _page.Submit();

            Assert.Equal(result, _page.GetResult());
        }

        [Theory, ClassData(typeof(InvalidTestData))]
        public void SuppliedInvalidData_ReturnsFailureReason(string matrix, string target, string message)
        {
            _page.EnterMatrix(matrix);
            _page.EnterTarget(target);
            _page.Submit();

            Assert.Equal(message, _page.GetResult());
        }

        public void Dispose()
        {
            _driver.Quit();
            _driver.Dispose();
            //GC.SuppressFinalize(this);
        }
    }
}